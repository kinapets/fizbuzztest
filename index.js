function fizzBuzzTest(start,end,cb){
    if(start == end + 1 ) return;
    var fizz = !Boolean(start % 3);
    var buzz = !Boolean(start % 5);
    var output = `${fizz ? "Fizz" : ""}${buzz ? "Buzz": ""}${!fizz & !buzz ? start: ""}`;
    cb(output);
    fizzBuzzTest(++start,end,cb)
}

fizzBuzzTest(1,100,console.log);
